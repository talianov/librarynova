<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    public function read($id){
        $book = Book::where('id', '=', $id)->firstOrFail();

        return view('book.read', compact('product', 'seo_title', 'meta_description', 'meta_keywords', 'twitter_description', 'og_title', 'og_image'));
    }
}
