<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $books = Book::orderBy('created_at', 'DESC')->paginate(8);

        return view('index', compact('books'));
    }

}
