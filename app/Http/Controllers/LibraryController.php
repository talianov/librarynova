<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function browse($id = null) {

        $books = Book::select();

        if(!empty($id))
        {
            $books->where('type_id', $id);
        }

        return view('book.browse', ['books' => $books->paginate(6)]);
    }

    public function read($id)
    {
        $book = Book::where('id', '=', $id)->firstOrFail();
        return view('book.read', compact('book'));
    }

}
