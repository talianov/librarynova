<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('created_at', 'DESC')->paginate(15);

        return view('index', compact('books'));
    }

}
