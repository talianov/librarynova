<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <title></title>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">

    <!-- Styles -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src='{{ asset('/js/bootstrap.min.js') }}'></script>
    <script src='{{ asset('/js/ofi.min.js') }}'></script>
    <script> objectFitImages(); </script>
    <style>
        .card-img-top {
            height: 21rem;
            object-fit: contain;
            object-position: center;
            font-family: 'object-fit: contain; object-position: center;';
        }
    </style>
</head>
<body>
    <div id="app">

        <div class="container pt-2 mb-4">
            <ul class="nav border-bottom">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/') }}">Главная</a>
                </li>
                @foreach(\App\Type::all() as $type)
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('browse', ['id' => $type->id]) }}">{{ $type->name }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

        @yield('content')


        <footer class="pb-5 mt-5 container border-top">

        </footer>

    </div>
</body>
</html>
