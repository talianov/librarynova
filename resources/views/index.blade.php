@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($books as $book)
                @include('book.card')
            @endforeach
        </div>
        <div class="mb-4"></div>
        {{ $books->links() }}
    </div>
@endsection
