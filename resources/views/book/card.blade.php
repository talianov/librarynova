<div class="col-6 col-md-4 col-lg-3">
    <div class="card mb-4 shadow-sm">
        <a href="{{ route('read', ['id' => $book->id]) }}">
            <img src="{{ url(Storage::url($book->image)) }}" class="card-img-top" alt="{{ $book->name }}">
        </a>
        <div class="card-body">
            <h5 class="card-title">{{ str_limit($book->name, 33) }}</h5>
            <p class="card-text">{{ $book->year }}, {{ $book->type->name }}</p>
            <a href="{{ route('read', ['id' => $book->id]) }}" class="btn btn-primary">Читать</a>
        </div>
    </div>
</div>
