@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3">
            <img src="{{ url(Storage::url($book->image)) }}" style="" class="img-fluid" alt="{{ $book->name }}">
        </div>
        <div class="col-9">
            <h5 class="card-title">{{ $book->name }}</h5>
            <h6 class="card-subtitle mb-2 text-muted"><a href="{{ route('browse', ['id' => $book->type_id]) }}">{{ $book->type->name }}</a></h6>
            <span>{{ $book->year }}</span>
        </div>
    </div>
</div>
@endsection
