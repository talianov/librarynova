@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            @forelse($books as $book)
                @include('book.card')
            @empty
                <div class="col">Ничего не найдено</div>
            @endforelse
        </div>
        <div class="mb-4"></div>
        {{ $books->links() }}
    </div>
@endsection
